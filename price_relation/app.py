from flask import Flask, render_template, request
from price_relations import main
from pymongo import MongoClient
#from flask.ext.script import Manager
import datetime

app = Flask(__name__)
#manager = Manager(app)


@app.route('/')
def entry_page() -> 'html':
    return render_template('entry.html',
                           the_title='欢迎使用比价及价格历史查询系统')


@app.route('/compare', methods=['POST'])
def search_products() -> str:
    word = request.form['word']
    title = '比价结果'
    titles = ('商品', '图片','价格', '链接', '店铺', '来源')
    results = main(word)
    return render_template('results.html',
                           the_word=word,
                           the_title=title,
                           the_row_titles=titles,
                           the_data=results,)

def get_history(id):
    # 定义shopping数据库jd1集合
    client = MongoClient()
    collection = client["shopping"]["jd1"]
    data = collection.find_one({'g_gid' : id})
    lables = []
    price = []
    for key,value in data.items():
        lables.append(key)
        price.append(value)
    return lables,price


@app.route('/history', methods=['POST'])
def history() -> str:
    id = request.form['id']
    title = '历史结果'
    data = get_history(id)
    lables,value = get_history(id)
    time_now = int(datetime.datetime.now().strftime("%Y%m%d"))
    lables = lables[1:]
    value = value[1:]
    lables[19] = '20190412'
    #titles = ('商品', '价格', '链接', '店铺', '来源')
    #print(data)
    return render_template('history.html',
                           good_name = value[6],
                           column = lables[19:],
                           values = value[19:])

if __name__ == '__main__':
    manager.run()
