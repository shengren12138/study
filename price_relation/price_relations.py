from  suning import crawl_suning as suning
from jd import crawl_jd as jd
from tianmao import crawl_tianmao as tianmao




def main(word):
    """ 默认爬取三个网站的3页,可以自己调整页数 """
    products_list = []
    # 京东数据
    jd(word, products_list)
    # 苏宁店数据
    suning(word, products_list)
    #天猫数据
    tianmao(word, products_list)
    # 排序书的数据
    products_list = sorted(products_list, key=lambda item: float(item['price']), reverse=False)
    for products in products_list:
        print(products)
    return products_list




if __name__ == '__main__':
    word = input('请输入商品名称：')
    main(word)

