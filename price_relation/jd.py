import  selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import re
import time
from pymongo import MongoClient
from selenium.webdriver.common.keys import Keys
from pyquery import PyQuery as pq
from urllib import parse




profile = webdriver.FirefoxProfile()
profile.set_preference("general.useragent.override", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0")
options = webdriver.FirefoxOptions()
options.add_argument('--headless')
options.add_argument('--disable-gpu')
browser = webdriver.Firefox(firefox_options=options,firefox_profile=profile,executable_path='/usr/local/bin/geckodriver')
wait = WebDriverWait(browser,10)


def search(keyword,products_list=[]):
    browser.get('https://www.jd.com/')
    # 找到输入的搜索框
    _input = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '#key'))
    )
    # 找到搜索按钮
    submit = WebDriverWait(browser, 10).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, "#search > div > div.form > button")))
    _input.send_keys(keyword)
    submit.click()
    time.sleep(3)
    parse_html()
    # 找到总页数
    try:
        close = browser.find_element_by_css_selector('.close-btn')
        close.click()
    except Exception as e:
        pass
        #print(e)
    finally:
            target = browser.find_element_by_css_selector('#J_bottomPage > span.p-skip > em:nth-child(1)')
            browser.execute_script("arguments[0].scrollIntoView();", target)
            time.sleep(3)
            total = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, '#J_bottomPage > span.p-skip > em:nth-child(1)')))
            # 对找到的总页数进行正则处理，并返回int类型的页数
            _total = total.text
            # print(_total)
            pattern = re.compile('\S\S(\d+).*?')
            result = re.search(pattern, _total)
            parse_html()
    return int(result.group(0)[1:])


def swipe_down(second):
    for i in range(int(second/0.1)):
        js = "var q=document.documentElement.scrollTop=" + str(400+200*i)
        browser.execute_script(js)
        time.sleep(0.1)
    js = "var q=document.documentElement.scrollTop=100000"
    browser.execute_script(js)
    time.sleep(0.2)



def next_page(page,products_list=[]):
    print('正在翻页', page)
    try:
        # 找到页数的输入框
        time.sleep(1)
        inputs = WebDriverWait(browser, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '#J_bottomPage > span.p-skip > input')))
        time.sleep(1)
        inputs.clear()
        # 找到确定的按钮
        submit = WebDriverWait(browser, 10).until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "#J_bottomPage > span.p-skip > a")))
        inputs.send_keys(page)
        submit.send_keys(Keys.ENTER)
        target = browser.find_element_by_css_selector('#J_bottomPage > span.p-skip > em:nth-child(1)')
        browser.execute_script("arguments[0].scrollIntoView();", target)  # 将页面下拉至底部休息3秒等待数据加载
        time.sleep(3)
        # 进行判定：高亮下的页数是否和输入框的一致
        # WebDriverWait(browser, 10).until(
        #     EC.element_to_be_clickable(
        #         (By.CSS_SELECTOR, '#bottom_pager > div > a.cur')), str(page))
        parse_html(products_list)
    except Exception as e:
        print(e.args)
        next_page(page,products_list)


def parse_html(products_list=[]):
    try:
        # 选择整个展示框
        swipe_down(2)
        print('******************')
        html = browser.page_source
        doc = pq(html)
        # 拿到所有的item，进行迭代，拿到所有商品的数据
        items = doc('#J_goodsList .gl-item').items()
        for item in items:
            text = str(item.find('.p-img'))
            try:
                price = float(item.find('.p-price').text().replace('\n', ' ').strip().replace('￥',''))
                picture = re.findall(r"src=\"(.*?).jpg", str(item))[0]
                picture = 'http:' + picture + '.jpg'
            except Exception as e:
                continue
            URL = (re.findall(r"href=\"(.*?)\">", text))
            if len(URL)==0:
                continue
            url = str(URL[0]).split('"')[0]
            detail_url = parse.urljoin('http://jd.com',url)

            #url1 = str(url)
            # if Price == "" or len(url)==0 :
            #     continue
           # price = float(re.findall(r"\d+\.?\d*",Price)[0])
            print(price,detail_url,picture)
            products_list.append({
                #'g_big_cate': "女装",
                #'g_cate': "衬衫女",
                'picture':picture,
                'price': price,
                'detail_url': detail_url,
                'description': item.find('.p-name.p-name-type-2').text().strip().replace('\n', ' '),
                'shop': item.find('.p-shop').text().replace('\n', ' '),
                'reference':'京东'
            })
            #if collection.insert(proucts):
                #print('正在保存', proucts.get('description'))
    except TimeoutError as err:
        print(err)
        #parse_html()

def crawl_jd(keyword,products_list=[]):
    try:
        print('京东网数据正在爬取')
        total = search(keyword)  # 得到的总页数
        for i in range(2, 3):
            next_page(i,products_list)
            time.sleep(2)
        print('京东网数据爬取完成')
    except Exception as e:
        print(e.args)
    finally:
        browser.close()
        return products_list


if __name__ == '__main__':
    crawl_jd('衣服',products_list=[])