from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import re
import time
from pymongo import MongoClient
from selenium.webdriver.common.keys import Keys
from pyquery import PyQuery as pq


profile = webdriver.FirefoxProfile()
profile.set_preference("general.useragent.override", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0")
options = webdriver.FirefoxOptions()
options.add_argument('--headless')
options.add_argument('--disable-gpu')
browser = webdriver.Firefox(firefox_options=options,firefox_profile=profile,executable_path='/usr/local/bin/geckodriver')

# # 进入浏览器设置
# options = webdriver.ChromeOptions()
# #谷歌无头模式
# options.add_argument('--headless')
# options.add_argument('--disable-gpu')
# options.add_argument('user-agent="Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20"')
# options.add_argument('lang=zh_CN.UTF-8')
# browser = webdriver.Chrome(chrome_options=options)
client = MongoClient()
collection = client["shopping"]["suning"]


def search(keyword,products_list=[]):
    # 请求苏宁易购首页
    browser.get('http://www.suning.com/')
    # 找到输入的搜索框
    _input = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '#searchKeywords'))
    )
    # 找到搜索按钮
    submit = WebDriverWait(browser, 10).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, "#searchSubmit")))
    _input.send_keys(keyword)
    submit.click()
    time.sleep(3)
    # 找到总页数
    try:
        close = browser.find_element_by_css_selector('.close-btn')
        close.click()
    except Exception as e:
        print(e)
    finally:
            target = browser.find_element_by_css_selector('#bottom_pager > div > span.page-more')
            browser.execute_script("arguments[0].scrollIntoView();", target)
            time.sleep(3)
            total = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, '#bottom_pager > div > span.page-more')))
            # 对找到的总页数进行正则处理，并返回int类型的页数
            _total = total.text
            # print(_total)
            pattern = re.compile('\S\S(\d+).*?')
            result = re.search(pattern, _total)
            parse_html()
    return int(result.group(0)[1:])


def swipe_down(second):
    for i in range(int(second/0.1)):
        js = "var q=document.documentElement.scrollTop=" + str(400+200*i)
        browser.execute_script(js)
        time.sleep(0.1)
    js = "var q=document.documentElement.scrollTop=100000"
    browser.execute_script(js)
    time.sleep(0.2)



def next_page(page,products_list=[]):
    print('正在翻页', page)
    try:
        # 找到页数的输入框
        time.sleep(1)
        inputs = WebDriverWait(browser, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '#bottomPage')))
        time.sleep(1)
        inputs.clear()
        # 找到确定的按钮
        submit = WebDriverWait(browser, 10).until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "#bottom_pager > div > a.page-more.ensure")))
        inputs.send_keys(page)
        submit.send_keys(Keys.ENTER)
        target = browser.find_element_by_css_selector('#bottom_pager > div > span.page-more')
        browser.execute_script("arguments[0].scrollIntoView();", target)  # 将页面下拉至底部休息3秒等待数据加载
        time.sleep(3)
        # 进行判定：高亮下的页数是否和输入框的一致
        WebDriverWait(browser, 10).until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, '#bottom_pager > div > a.cur')), str(page))
        parse_html(products_list)
    except Exception as e:
        print(e.args)
        next_page(page,products_list)


def parse_html(products_list=[]):
    try:
        # 选择整个展示框
        WebDriverWait(browser, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '#product-list .item-wrap')))
        # product-list
        swipe_down(4)
        html = browser.page_source
        doc = pq(html)
        # 拿到所有的item，进行迭代，拿到所有商品的数据
        items = doc('#product-list .item-wrap').items()
        for item in items:
            text = str(item.find('.title-selling-point'))
            Price = str(item.find('.def-price').text().replace('\n', ' '))
            url = (re.findall(r"href=\"(.*?)\">", text))
            picture = re.findall(r"src=\"(.*?).jpg",str(item))
            if picture:
                picture = picture[0]
            else :
                continue
            #url1 = str(url)
            if Price == "" or len(url)==0:
                continue
            price = float(re.findall(r"\d+\.?\d*",Price)[0])
            if len(url[0])>100:
                continue
            detail_url = "http:" + url[0]
            picture = 'http:' + picture + '.jpg'


            print(price,detail_url)
            products_list.append({
                'picture':picture,
                'price': price,
                'detail_url': detail_url,
                'description': item.find('.title-selling-point').text().strip().replace('\n', ' '),
                'shop': item.find('.store-stock').text().replace('\n', ' '),
                'reference':'苏宁'
            })
            #if collection.insert(proucts):
                #print('正在保存', proucts.get('description'))
    except TimeoutError:
        parse_html()

def crawl_suning(keyword,products_list=[]):
    try:
        print('苏宁网数据正在爬取')
        total = search(keyword)  # 得到的总页数
        for i in range(2, 3):
            next_page(i,products_list)
            time.sleep(4)
        print('苏宁网数据爬取完成')
    except Exception as e:
        print(e.args)
    finally:
        browser.close()


if __name__ == '__main__':
    crawl_suning('衣服',products_list=[])










