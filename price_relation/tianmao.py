from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import re
import time
import random
from pymongo import MongoClient
from selenium.webdriver.common.keys import Keys
from pyquery import PyQuery as pq
from selenium.webdriver import ActionChains


profile = webdriver.FirefoxProfile()
#profile.set_preference("general.useragent.override", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0")
options = webdriver.FirefoxOptions()
options.add_argument('--headless')
options.add_argument('--disable-gpu')
browser = webdriver.Firefox(firefox_options=options,firefox_profile=profile,executable_path='/usr/local/bin/geckodriver')

# # 进入浏览器设置
# options = webdriver.ChromeOptions()
# #谷歌无头模式
# options.add_argument('--headless')
# options.add_argument('--disable-gpu')
# options.add_argument('user-agent="Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20"')
# options.add_argument('lang=zh_CN.UTF-8')
# browser = webdriver.Chrome(chrome_options=options)
client = MongoClient()
collection = client["shopping"]["tianmao"]


def login(url):
    # 打开网页
    browser.get(url)

    try:
        close = browser.find_element_by_css_selector('#J_Quick2Static')
        close.click()
        #browser.execute_script("arguments[0]", input)
    except Exception as e:
        print(e)
    user = browser.find_element(By.ID, 'TPL_username_1')
    password = browser.find_element(By.ID, 'TPL_password_1')
    user.send_keys("15516024695")
    time.sleep(random.random() * 2)
    password.send_keys("qiang868")
    time.sleep(random.random() * 1)
    browser.execute_script("Object.defineProperties(navigator,{webdriver:{get:() => false}})")
    action = ActionChains(browser)
    time.sleep(random.random() * 1)
    butt = browser.find_element(By.ID, 'nc_1_n1z')
    browser.switch_to.frame(browser.find_element(By.ID, '_oid_ifr_'))
    browser.switch_to.default_content()
    action.click_and_hold(butt).perform()
    action.reset_actions()
    action.move_by_offset(280, 0).perform()
    time.sleep(random.random() * 1)
    button = browser.find_element(By.ID, 'J_SubmitStatic')
    time.sleep(random.random() * 2)
    button.click()

    # # 自适应等待，点击密码登录选项
    # browser.implicitly_wait(30)  # 智能等待，直到网页加载完毕，最长等待时间为30s
    # browser.find_element_by_xpath('//*[@class="forget-pwd J_Quick2Static"]').click()
    #
    # # 自适应等待，点击微博登录宣传
    # browser.implicitly_wait(30)
    # browser.find_element_by_xpath('//*[@class="weibo-login"]').click()
    #
    # # 自适应等待，输入微博账号
    # browser.implicitly_wait(30)
    # browser.find_element_by_name('username').send_keys(weibo_username)
    #
    # # 自适应等待，输入微博密码
    # browser.implicitly_wait(30)
    # browser.find_element_by_name('password').send_keys(weibo_password)
    #
    # # 自适应等待，点击确认登录按钮
    # browser.implicitly_wait(30)
    # browser.find_element_by_xpath('//*[@class="btn_tip"]/a/span').click()

    # 直到获取到淘宝会员昵称才能确定是登录成功
    taobao_name = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR,
                                                                  'div.s-name > a:nth-child(1)')))
    # 输出淘宝昵称
    print(taobao_name.text)


def search(keyword):
    # 请求首页
    browser.get('https://list.tmall.com/')
    # 找到输入的搜索框
    _input = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '#mq'))
    )
    # 找到搜索按钮
    submit = WebDriverWait(browser, 10).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, ".mallSearch-input > button:nth-child(3)")))
    _input.send_keys(keyword)
    submit.click()
    time.sleep(3)
    # 找到总页数
    # try:
    #     close = browser.find_element_by_css_selector('.close-btn')
    #     close.click()
    # except Exception as e:
    #     print(e)
    # finally:

    target = browser.find_element_by_css_selector('#content > div > div.ui-page > div > b.ui-page-skip > form')
    browser.execute_script("arguments[0].scrollIntoView();", target)
    time.sleep(3)
    total = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, '#content > div > div.ui-page > div > b.ui-page-skip > form')))
    # 对找到的总页数进行正则处理，并返回int类型的页数
    _total = total.text
    print(_total)
    print(_total)
    pattern = re.compile('\S\S(\d+).*?')
    result = re.search(pattern, _total)
    parse_html()
    return  int(result.group(0)[1:])


def swipe_down(second):
    for i in range(int(second/0.1)):
        js = "var q=document.documentElement.scrollTop=" + str(400+200*i)
        browser.execute_script(js)
        time.sleep(0.1)
    js = "var q=document.documentElement.scrollTop=100000"
    browser.execute_script(js)
    time.sleep(0.2)



def next_page(page):
    print('正在翻页', page)
    try:
        # 找到页数的输入框
        time.sleep(1)
        inputs = WebDriverWait(browser, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '#content > div > div.ui-page > div > b.ui-page-skip > form > input.ui-page-skipTo')))
        time.sleep(1)
        inputs.clear()
        # 找到确定的按钮
        submit = WebDriverWait(browser, 10).until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "#content > div > div.ui-page > div > b.ui-page-skip > form > button")))
        inputs.send_keys(page)
        submit.send_keys(Keys.ENTER)
        target = browser.find_element_by_css_selector('#content > div > div.ui-page > div > b.ui-page-skip > form')
        browser.execute_script("arguments[0].scrollIntoView();", target)  # 将页面下拉至底部休息3秒等待数据加载
        time.sleep(3)
        # 进行判定：高亮下的页数是否和输入框的一致
        WebDriverWait(browser, 10).until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, '#content > div > div.ui-page > div > b.ui-page-num > b.ui-page-cur')), str(page))
        parse_html()
    except Exception as e:
        print(e.args)
        next_page(page)


def parse_html(url,products_list=[]):
    browser.get(url)
    try:
        # 选择整个展示框
        try:
            #iframe = browser.find_element_by_xpath('//iframe')  # 找到“嵌套”的iframe
            #browser.switch_to.frame(iframe)  # 切换到iframe
            swipe_button = browser.find_element_by_css_selector('#nc_1_n1z')  # 获取滑动拖动控件

            # 模拟拽托
            action = ActionChains(browser)  # 实例化一个action对象
            action.click_and_hold(swipe_button).perform()  # perform()用来执行ActionChains中存储的行为
            action.reset_actions()
            action.move_by_offset(580, 0).perform()  # 移动滑块
        except Exception as e:
            print(e)
        finally:
                WebDriverWait(browser, 20).until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, '#J_ItemList')))
                # product-list
                swipe_down(2)
                html = browser.page_source
                doc = pq(html)
                # 拿到所有的item，进行迭代，拿到所有商品的数据
                items = doc('#J_ItemList .product').items()
                for item in items:
                    text = str(item.find('.productImg-wrap'))
                    sales_comments = str(item.find('.productStatus'))
                    Price = str(item.find('.productPrice').text().replace("¥", "").replace(" ", "").replace('\n', "").replace('\r', ""))
                    url = (re.findall(r"href=\"(.*?)\;sku", text))
                    pictures = re.findall(r"src=\"(.*?).jpg", str(item))
                    if sales_comments and Price and url and pictures:
                        sale = (re.findall(r'<em>(.*?)笔',sales_comments)[0])
                        picture = 'http:' + pictures[0] + '.jpg'
                        if re.findall(r'(.*?)万',sale):
                            sale = int(float(re.findall(r'(.*?)万',sale)[0])*10000)
                        else:
                            sale = int(sale)
                        comment = (re.findall(r'\">(.*?)<\/a>',sales_comments)[0])
                        if re.findall(r'(.*?)万',comment):
                            comment = int(float(re.findall(r'(.*?)万',comment)[0])*10000)
                        else:
                            comment = int(comment)
                        price = float(re.findall(r"\d+\.?\d*",Price)[0])
                        detail_url = "http:" + url[0]
                        print(price,detail_url,sale,picture)
                        products_list.append({
                            #'g_big_cate': bag_cate,
                            #'g_cate': cate,
                            'price': price,
                            'picture':picture,
                            #'comment': comment,
                            #'monthly_sale': sale,
                            'detail_url': detail_url,
                            'description': item.find('.productTitle').text().strip().replace('\n', ' '),
                            'shop': item.find('.productShop-name').text(),
                            'reference':'天猫'
                        })
                        #collection.insert(proucts)
                        #print('正在保存', proucts.get('description'))
                    else:
                        continue

    except TimeoutError:
        parse_html()

def crawl_tianmao(keyword='T恤女',products_list=[]):
    # urls = ['T恤女','T恤男','衬衫女','衬衫男']
    # #login(url)
    # for url in  urls:
    #     if url == 'T恤女':
    #         bit_cate = '女装'
    #         cate = 'T恤女'
    #     elif url == 'T恤男':
    #         bit_cate = '男装'
    #         cate = 'T恤男'
    #     elif url == '衬衫女':
    #         bit_cate = '女装'
    #         cate = '衬衫女'
    #     else:
    #         bit_cate = '男装'
    #         cate = '衬衫男'
        print('天猫网数据正在爬取')
        url = "https://list.tmall.com/search_product.htm?type=pc&q={}&totalPage=80&jumpto=".format(keyword)
        #bit_cate = '男装'
        #cate = '衬衫男'
        try:
            for i in range(1, 3):
                #parse_html(url + str(i),bit_cate,cate)
                parse_html(url + str(i),products_list)
                #time.sleep(2)
                print("现在第" + str(i) + "页")
            print('天猫网数据爬取完成')
        except Exception as e:
            print(e.args)
        finally:
            browser.close()


if __name__ == '__main__':
    crawl_tianmao('衣服',products_list=[])










