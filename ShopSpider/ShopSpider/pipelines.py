# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import  re
from pymongo import MongoClient

class ShopspiderPipeline(object):
    def open_spider(self,spider):
        client = MongoClient()
        #定义shopping数据库jd集合
        self.collection1 = client["shopping"]["jd1"]
        self.collection2 = client["shopping"]["taobao"]

    def process_item(self, item, spider):
        #item["g_detail_name"] = self.process_name(item["g_detail_name"])

        #存入mongodb内
        if spider.name == "Jd1":
            print(item["price"])
            #item["weight"] = self.process_weight(item["weight"])
            #self.collection1.update({'g_gid':item["g_gid"]},{'$set':{'20190529':item["price"]}})
        else:
            self.collection2.insert(dict(item))
        return item

    def process_weight(self,content):
        content = str(content)
        if content.find('kg') == -1:
            #/d+ 匹配数字 /.?匹配小数点 \d*匹配小数点后面的数
            weight = float(re.findall(r"\d+\.?\d*",content)[0])
            weight = weight/1000
        else:
            weight = float(re.findall(r"\d+\.?\d*",content)[0] )
        return weight




