# -*- coding: utf-8 -*-
import scrapy
from copy import deepcopy
import json

class Jd1Spider(scrapy.Spider):
    name = 'Jd1'
    allowed_domains = ['jd.com','p.3.cn']
    start_urls = ['https://p.3.cn/prices/mgets?skuIds=100000287133&pdtk=&pduid']
    count = 0

    def parse(self, response):
        item = {}
        with open('/home/liu/Projects/ShopSpider/ShopSpider/Id.txt', 'r') as file:
            ids = file.readlines()
        for id in ids:
            print(id)
            item["g_gid"] = id.strip()
            url = "http://p.3.cn/prices/mgets?skuIds=J_" + item["g_gid"]+ "&pdtk=&pduid"
            yield scrapy.Request(
                url,
                callback=self.parse_price,
                meta={"item": deepcopy(item)}
            )
    def parse_price(self, response):
        item = response.meta["item"]
        data = json.loads(response.body.decode())
        if data[0]["p"]:
            item["price"] = float(data[0]["p"])
        self.count = self.count +1
        print(item["g_gid"],item["price"],self.count)
        yield item






