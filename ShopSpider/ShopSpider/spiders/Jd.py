# -*- coding: utf-8 -*-
import scrapy
from copy import deepcopy
import re
import json
class JdSpider(scrapy.Spider):
    name = 'Jd'
    allowed_domains = ['jd.com','3.cn']
    start_urls = ['http://www.jd.com/allSort.aspx']

    def parse(self, response):
        item={}
        dl_list = response.xpath("//div[@clstag='secondtype|keycount|allfenlei|flmc_9']/dl")[0:2]
        #获取衣服前两个总分类即女装、男装
        for dl in dl_list:
            item["g_big_cate"] = dl.xpath("./dt/a/text()").extract_first()
            #分别获取男装、女装里面的小分类
            a_list = dl.xpath("./dd/a")[0:2]
            for a in a_list:
                # item["g_big_cate"] = cate
                item["g_href"] = "http:" + a.xpath("./@href").extract_first()
               # item["g_gid"] = re.findall(r'http://item.jd.com/(.*?).html',item["g_href"])[0]
                item["g_name"] = a.xpath("./text()").extract_first()
                yield scrapy.Request(
                    item["g_href"],
                    callback=self.parse_goods_list,
                    meta={"item":deepcopy(item)}
                )
    #处理每个小分类定义的方法
    def parse_goods_list(self,response):
        item = response.meta["item"]
        max_page = int(re.findall("<em>共<b>(.*?)</b>",response.body.decode())[0])
        #当前页面的商品列表
        li_list = response.xpath("//div[@id='plist']/ul/li")
        for li in li_list:
            item["g_shop"] = li.xpath("./div/div[7]/@data-shop_name").extract_first()
            #详细商品的网址
            item["g_detail_href"] = "http:" + li.xpath("./div/div[1]/a/@href").extract_first()
            #item["g_gid"] = str(re.findall(r"item.jd.com/(.*?).html",item["g_detail_href"])[0])
            #print(item["g_detail_href"])
            if item["g_detail_href"] is not "http://www.jd.com/?d":
                yield scrapy.Request(
                    item["g_detail_href"],
                    callback=self.parse_goods_detail,
                    meta={"item":deepcopy(item)}
                )

        #实现下一页的功能
        next_page = int(re.findall("\"pageNo\":\"(.*?)\",",response.body.decode())[0]) + 1
        next_url = item["g_href"] + "&page={}".format(next_page)
        if next_page <= max_page:
            yield scrapy.Request(
                next_url,
                callback=self.parse_goods_list,
                meta={"item":deepcopy(item)}
            )

    def parse_goods_detail(self,response):
        item = response.meta["item"]
        item["g_gid"] = str(re.findall(r"item.jd.com/(.*?).html", item["g_detail_href"])[0])
        url = "http://p.3.cn/prices/mgets?skuIds=J_" + item["g_gid"] + "&pdtk=&pduid"
        if item["g_gid"] is not None:
            yield scrapy.Request(
                url,
                callback=self.parse_price,
                meta={"item":deepcopy(item)}
            )

        item["g_gid"] = response.xpath("//div[@class='p-parameter']/ul[2]/li[2]/@title").extract_first()
        data = response.body.decode('gb2312','ignore')
        print(item["g_detail_href"])
        item["g_detail_name"] = re.findall(r">商品名称：(.*?)<", data)[0]
        style = re.findall(r">风格：(.*?)<",data)
        item["style"] = self.JudgeIsNone(style)
        materials = re.findall(r">主要材质：(.*?)<",data)
        item["materials"] = self.JudgeIsNone(materials)
        g_data = re.findall(r">上市时间：(.*?)<",data)
        item["g_date"]  = self.JudgeIsNone(g_data)
        producer = re.findall(r">商品产地：(.*?)<",data)
        item["producer"] = self.JudgeIsNone(producer)
        weigth = re.findall(r">商品毛重：(.*?)<",data)
        if weigth is None:
            item["weight"] = "500.00g"
        else:
            item["weight"] = weigth[0]
        url = "https://sclub.jd.com/comment/productPageComments.action?callback=fetchJSON_comment98vv288&productId={}&score=0&sortType=5&page=1&pageSize=10".format(item["g_gid"])

        if item["g_gid"] is not None:
            yield scrapy.Request(
                url,
                callback=self.parse_comments,
                meta={"item":deepcopy(item)}
            )

    def JudgeIsNone(self,str):
        if(len(str)):
            return str[0]
        else:
            return "其他"

    #从中获取评论数，好评数，中评数，差评数等
    def parse_comments(self,response):
        item = response.meta["item"]
        data = response.body.decode('gb2312','ignore')
        item["GoodRate"] =  int(re.findall("\"goodRateShow\":(.*?),",data)[0])
        item["PoorRate"] = int(re.findall("\"poorRateShow\":(.*?),",data)[0])
        item["AverageScore"] = int(re.findall("\"averageScore\":(.*?),",data)[0])
        item["GoodComments"] = int(re.findall("\"goodCount\":(.*?),",data)[0])
        item["PoorComments"] = int(re.findall("\"poorCount\":(.*?),",data)[0])
        item["AllComments"] = int(re.findall("\"commentCount\":(.*?),",data)[0])
        item["GeneralComments"] = int(re.findall("\"generalCount\":(.*?),",data)[0])
        url = "http://p.3.cn/prices/mgets?skuIds=J_" + item["g_gid"] +"&pdtk=&pduid"
        if item["g_gid"] is not None:
            yield scrapy.Request(
                url,
                callback=self.parse_price,
                meta={"item":deepcopy(item)}
            )

    def parse_price(self,response):
        item = response.meta["item"]
        data = json.loads(response.body.decode())
        if data[0]["p"]:
            item["price"] = float(data[0]["p"])
        print(item)
        yield item





