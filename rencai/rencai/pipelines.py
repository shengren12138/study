# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from pymongo import MongoClient

class RencaiPipeline(object):
    def open_spider(self, spider):
        client = MongoClient()
        # 定义shopping数据库jd集合
        self.collection = client["Teachers"]["csu"]#中南大学

    def process_item(self, item, spider):
        self.collection.insert(dict(item))
        return item



