# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy import signals
import random


class RencaiSpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    def process_request(self, request, spider):
        User_Agent = random.choice(spider.settings.get("USER_AGENT"))
        #request.headers["User-Agent"] = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/65.0"
        request.headers["User-Agent"] = User_Agent
        #proxy = self.get_random_proxy()
        # request.meta["proxy"] = "http://42.51.42.201:808"
        # request.meta["proxy"] = "http://221.176.206.29:8060"
        # request.meta["proxy"] = "http://42.51.42.201:808"
        # request.meta["proxy"] = "http://218.28.96.196:8060"
        # request.meta["proxy"] = "http://124.202.166.171:82"

    # self._check_time()
    # request.meta["proxy"] = self._get_random_proxy()
    # print(request.meta["proxy"] )

        request.meta["proxy"] = random.choice(spider.settings.get("IPS1"))
        print('ip' + request.meta["proxy"])

    def process_response(self, request, response, spider):
        # 根据返回的状态码来验证ip是否可用
        if response.status != 200:
            print('ip' + request.meta["proxy"] + '无效')
            # proxy = self.get_random_proxy()
            # request.meta["proxy"] = random.choice(spider.settings.get("IPS"))
            return request
        return response

    # def _check_time(self):
    #     if datetime.datetime.now() >= self.miss_datatime:
    #         self._get_random_proxy()

    #
    def _get_random_proxy(self):
        # resp = requests.get(self.url)
        # info = json.loads(resp.text)
        # proxy = info['data'][0]
        # self.proxy = proxy['proxyip']
        # self.miss_datatime = datetime.datetime.now() + datetime.timedelta(minutes=1)
        with open('/home/liu/Projects/ShopSpider/ShopSpider/proxies.txt', 'r') as file:
            proxies = file.readlines()
        proxy = random.choice(proxies).strip()
        return proxy