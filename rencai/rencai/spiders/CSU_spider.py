# -*- coding: utf-8 -*-
import scrapy
from copy import deepcopy
import re
import json
import requests
headers = {
    'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Encoding':'gzip, deflate',
    'Accept-Language':'zh,en-US;q=0.7,en;q=0.3',
    'Connection':'keep-alive',
    'Cookie':'JSESSIONID=BDBD2786E342A200F3D221AE3F182B5B; JSESSIONID=6AFA326D583BA9BC0B7F7A0FF20ADBDA',
    'Host':'faculty.csu.edu.cn',
    'Pragma':'no-cache',
    'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0',
}


class CsuSpiderSpider(scrapy.Spider):
    name = 'CSU_spider'
    allowed_domains = ['csu.edu.cn']
    start_urls = ['http://faculty.csu.edu.cn/xueyuan.jsp?urltype=tree.TreeTempUrl&wbtreeid=1014']


    def parse(self, response):
        item={}
        li_list = response.xpath("//div[@class='TabbedPanelsContentGroup']/div/ul/li")
        #提取各学院
        for li in li_list:
            academy = li.xpath("./a/text()").extract_first()
            item["academy"] = re.findall(r"(.*?)\d+",academy)[0].replace('(','')
            academy_url =  "http://faculty.csu.edu.cn/" + li.xpath("./a/@href").extract_first()
            number = int(re.findall(r"(\d+)",academy)[0])
            if(number > 0):
                yield scrapy.Request(
                academy_url,
                callback=self.parse_academys_list,
                meta={"item":deepcopy(item)}
                )
            else :
                continue





    def parse_academys_list(self,response):
        item = response.meta["item"]
        li_list = response.xpath("//div[@id='sucai2']/ul/li")
        for li in li_list:
            item['url'] = li.xpath("./a/@href").extract_first()
            item['name'] = li.xpath('./a/text()').extract_first()
            yield scrapy.Request(
                item['url'],
                callback=self.parse_detail,
                meta={"item":deepcopy(item)}
            )

    def parse_detail(self, response):
        item = response.meta["item"]
        photo = response.xpath("//div[@class='photo']//td[@align='center']/img/@src").extract_first()
        photo1 = response.xpath("//div[@class='teacher_mid_topLimg fl']/script[2]/text()").extract_first()

        photo2 = response.xpath("//div[@class='t_photo']/script[2]/text()").extract_first()
        photo3 = response.xpath("//div[@class='toimg']/img/@src").extract_first()
        if (photo):
            item["photo"] = "http://faculty.csu.edu.cn"+ photo
            p_list = response.xpath("//div[@class='jbqk']/p")
            #item["position"] = p_list[0].xpath("./text()").extract_first()
            #print(item["photo"])
            #提取职称、电子邮件等
            for p in p_list:
                p_text = p.xpath("./text()").extract_first().replace("\n","").strip()
                job_time = re.findall(r"入职时间：(.*?)$",p_text,re.S)
                degree = re.findall(r"学位：(.*?)$",p_text,re.S)
                graduate_school = re.findall(r"毕业院校：(.*?)$",p_text,re.S)
                email = re.findall(r"电子邮件(.*?)$",p_text,re.S)
                postiontion = re.findall(r"[教授|导师]$",p_text,re.S)
                if(job_time):
                    item["job_time"] = job_time[0]
                    #print(p_text)
                    #print(job_time)
                elif(degree):
                    item["degree"] = degree[0]
                    #print(p_text)
                    #print(degree)
                elif(graduate_school):
                    item["graduate_school"] = graduate_school[0]
                elif(email):
                    encrypt_content = p.xpath("./span/text()").extract_first()
                    encrypt_url = "http://faculty.csu.edu.cn/system/resource/tsites/tsitesencrypt.jsp?id=_tsites_encryp_tsteacher_tsemail&content={}&mode=8".format(encrypt_content)

                    # yield scrapy.Request(
                    #     encrypt_url,
                    #     callback=self.parse_email,
                    #     meta={"item":deepcopy(item)}
                    # )
                    headers['Referer'] = item['url']
                    print(encrypt_url)
                    data = json.loads(requests.get(encrypt_url,headers=headers).text)
                    item["email"] = data["content"]
                elif(postiontion):
                    item["postiontion"] = str(p_text).replace(" ","")
                else:
                    pass
            content_list =response.xpath("//div[@class='content']/div")
            if (content_list):
                #print(content_list)
                # 提取页面主要介绍、研究方向、教育工作经历
                for content in content_list:
                    unkonw = content.xpath("./h2/text()").extract_first()
                    if (re.findall(r"个人简历$", unkonw)):
                        item["resume"] = str(content.xpath("string(.)").extract_first()).replace("\xa0", "")
                    elif (re.findall(r"研究方向$", unkonw)):
                        item["direction"] = str(content.xpath("string(.)").extract_first()).replace("\n"," ").strip().replace("&nbsp","").replace("  ","").replace("\xa0","")
                    elif (re.findall(r"教育经历$", unkonw)):
                        item["education_experence"] = str(content.xpath("string(.)").extract_first()).replace("\n"," ").strip().replace("&nbsp","").replace("  ","").replace("\xa0","")
                    elif (re.findall(r"工作经历$", unkonw)):
                        item["job_experence"] = str(content.xpath("string(.)").extract_first()).replace("\n"," ").strip().replace("&nbsp","").replace("  ","").replace("\xa0","")
            else:
                pass
            print(item)
            yield item
        #处理第二种界面
        elif (photo1):
            photo1 = re.findall(r"_pic.addimg\(\"(.*?)\",\"\",\"", photo1)[0]
            item["photo"] = "http://faculty.csu.edu.cn"+ photo1
            p_list = response.xpath("//div[@class='teacher_mid_topLinpro fl']/ul/li")
            for p in p_list:
                p_text = str(p.xpath("./text()").extract_first()).replace("\n","").strip()
                degree = re.findall(r"学 位：(.*?)$",p_text,re.S)
                postiontion = re.findall(r"职 称：(.*?)$",p_text,re.S)
                if(degree):
                    item["degree"] = degree[0]
                elif(postiontion):
                    item["postiontion"] = str(p_text).replace(" ","")
            br_list =response.xpath("//div[@id='sucai']/br")
            for br in br_list:
                if(br_list):
                    br_text = str(br.xpath("/text()").extract_first())
                    email = re.findall(r"电子信箱：$",br_text)
                    if(email):
                        encrypt_content = br.xpath("/span/span/text()").extract_first()
                        if(len(encrypt_content > 15)):
                            encrypt_url = "http://faculty.csu.edu.cn/system/resource/tsites/tsitesencrypt.jsp?id=_tsites_encryp_tsteacher_tsemail&content={}&mode = 8".format(encrypt_content)
                            headers['Referer'] = item['url']
                            data = json.loads(requests.get(encrypt_url, headers=headers).text)
                            item["email"] = data["content"]
                        else:
                            continue
                    # yield scrapy.Request(
                    #     encrypt_url,
                    #     callback=self.parse_email,
                    #     meta={"item":deepcopy(item)}
                    #
                   # print(encrypt_url)

            resume = response.xpath("//div[@class='teacher_mid_midLLan fl']")
            if (resume):
                item["resume"] = str(resume.xpath("string(.)").extract_first()).replace("\xa0", "")
            else:
                pass
            print(item)
            yield item
        elif (photo2):
            photo2 = re.findall(r"_pic.addimg\(\"(.*?)\",\"\",\"", photo2)[0]
            item["photo"] = "http://faculty.csu.edu.cn"+ photo2
            #print(item["photo"])

            yield item

        elif(photo3):
            item["photo"] = "http://faculty.csu.edu.cn"+ photo3
            #print(item["photo"])
            yield item
        else:
            pass



    # def parse_email(self,response):
    #     item = response.meta["item"]
    #     data = json.loads(response.body.decode())
    #     item["email"] = data["content"]
