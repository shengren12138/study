import requests
from lxml import etree
import threading
from queue import Queue
from fake_useragent import UserAgent
import re
import csv

class Product(threading.Thread):
    def __init__(self, i, page_queue):
        #继承父类init:
        super().__init__()
        self.i = i
        self.page_queue = page_queue

    def run(self):
        while True:
            #队列为空时退出
            if self.page_queue.empty():
                break
            try:
                q = self.page_queue.get(block=False)
                #说明需要处理游戏信息
                print(len(q))
                if len(q) > 20 and len(q) < 90:

                    self.get_html(q,method=1)
                else:
                    self.get_html(q,method=2)
            except:
                pass
    #请求,对于游戏信息直接存储
    def get_html(self, url,method):
        ua = UserAgent()
        game = []
        user_Agent = ua.random
        headers = {"User-Agent": user_Agent}
        if method == 1:
            response = requests.get(url=url, headers=headers)
            response.encoding='utf-8'
            res = response.text
            tree = etree.HTML(res)
            #按照给定顺序append到列表中并写入csv文件中，使用数据时要指定字段
            game_id =  re.findall('.*/a/(.*?).htm',url)[0]
            game.append(game_id)#game_id
            game_name = tree.xpath('//div[@class="gameDesc"]/div[1]/h1/text()')[0]#game_name
            game.append(game_name)
            logo = "http:" + tree.xpath('//div[@class="gameDesc"]/img/@src')[0]
            game.append(logo)
            text = tree.xpath('//div[@class="pd30 tab1"]/div[@class="txtArea"]')
            introduce1 = text[0].xpath('./div/div/text()')[0]
            if len(introduce1) > 65:
                introduce = text[1].xpath('./div/div/text()')[0]
                game.append(introduce)
            else:
                introduce = introduce1
                game.append(introduce)
            score = float(tree.xpath('//div[@class="grade"]/div[1]/p[2]/text()')[0])
            print(score)
            game.append(score)
            commnet_count = float(str(tree.xpath('//div[@class="grade"]/div[1]/p[4]/text()')[0]).replace('人',''))
            game.append(commnet_count)
            print(game)
            with open('game.csv','a',newline='') as csv_file:
                csv_writer = csv.writer(csv_file)
                csv_writer.writerow(game)

        else:
            response = requests.get(url=url, headers=headers)
            response.encoding = 'utf-8'
            res = response.json()
            response_q.put(res)




class Consumer(threading.Thread):
    def __init__(self, j):
        super().__init__()
        self.j= j

    def run(self):
        while True:
            #停止条件：(1)消费者任务队列为空（2）生产者线程都退出；
            if response_q.empty() and flag :
                break

            try:
                respnse = response_q.get(block=False)
                self.parse_html(respnse)
            except:
                pass

    # 二、解析函数：
    def parse_html(self, response):
        contents = response["content"]
        print(contents)
        for comment in contents:
            user = []
            comment_id =comment["id"]
            user.append(comment_id)
            user_id = comment["uid"]
            user.append(user_id)
            user_name = comment["username"]
            user.append(user_name)
            portrait = comment["time"]
            user.append(portrait)
            content = str(comment["comment"]).replace("<br>",'')
            user.append(content)
            like_count = comment["good_num"]
            user.append(like_count)
            reply_count = comment["num"]
            user.append(reply_count)
            #print(user)
            with lock:
                with open('comment.csv','a',newline='') as csv_file:
                    csv_writer = csv.writer(csv_file)
                    csv_writer.writerow(user)




#或许游戏列表
def GetGameList(url,user_Agent):
    headers = {"User-Agent": user_Agent}
    response = requests.get(url, headers=headers).text
    tree = etree.HTML(response)
    #提取所有包含游戏信息的li标签
    games_id = []
    li_list = tree.xpath('//ul[@class="ranking-game ranking-list"]/li')
    for li in li_list:
        href = li.xpath('./a/@href')[0]
        id = re.findall(".*/a/(.*?).htm",href)[0]
        games_id.append(id)
    return games_id





lock = threading.Lock()
response_q = Queue()#用于处理评论信息
flag = False #标志生产者线程是否全部都死了

#用于控制爬取评论数目，过多时请使用ip代理池，防止被封ip
N = 10

if __name__ == '__main__':

    #从第三方库获取userAgent,伪造浏览器
    ua=UserAgent()
    user_Agent=ua.random
    url = "https://www.3839.com/top/hot.html"
    #先获取游戏目录,游戏数目量不大可直接使用简单爬虫获取,由于评论数不能从总页面提取到，需要访问每个游戏页面进行提取
    #如果使用数据库的话，除了评论人数，均可以在排行榜里面爬取，最后爬取每个页面评论数，进行数据匹配
    game_id = GetGameList(url,user_Agent)

    #创建生产者队列
    page_queue = Queue()
    #测试时设置抓取前10页评论
    for id in game_id:
        game_url = "https://www.3839.com/a/" + id + ".htm"
        #page_queue.put(game_url)
        for i in range(1,N+1):
            comment_url = "https://www.3839.com/cdn/comment/view_v2-ac-json-pid-1-fid-{0}-p-{1}-order-1-htmlsafe-1-urltype-1-audit-1.htm".format(id,i)
            page_queue.put(comment_url)
    #创建起线程
    product_name = ['p1', 'p2', 'p3']
    consumer_name = ['c1', 'c2', 'c3']
    p_tread = []
    c_tread = []

    #生产者进程
    for i in product_name:
        p = Product(i,page_queue)
        p.start()
        p_tread.append(p)
    #消费者进程

    for j in consumer_name:
        c = Consumer(j)
        c.start()
        c_tread.append(c)


    for thredi in p_tread:
        thredi.join()

    flag = True
    for thredl in  c_tread:
        thredi.join()


