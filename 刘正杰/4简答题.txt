1. 简述scrapy的运行机制
答：scrapy分为5+2，引擎（Scarpy Engine） 调度器（Scheduler）,下载器（Downloader）,爬虫（Spider）,管道（Item Pipeline），两个中间件，下载中间件，和爬虫中间件
先给定一初始url，调度器将处理的Request请求转给引擎，下载器从引擎提取request，并把响应的responses传给引擎，爬虫从引擎获取response,进行相应处理，提取到item通过引擎传给item piplin ,提取到新的url通过引擎传给调度器，至于两个中间件，下载中间件位于引擎和下载器中间，爬虫中间件位于引擎与爬虫中间，根据用户需求可自己定义
2. scrapy中如何定义一个 Downloader Middleware
答：先将settings中将Downloader Middleware 打开，默认是关闭，然后在middleware中定义downloader Middleware ,需要重新编写两个方法process_request与process_response。

3. scrapy去重原理和scrapy-redis去重原
答：scrapy去重原理：Scrapy定义了一个dupefilters.py去重器，并且dont_filter默认设置为False，启动去重，scrapy会将每一个url,进行加密得到一个指纹，并将指纹和set()集合进行对比，如果set中已经存在，则不访问该url,如果没有，将该url放入队列当中，等待被访问，并将该指纹写入set集合
scrapy-redis去重复：通过 sha1 加密，把请求体，请求方式，请求 url 放在一起。然后进行 16 进制的转义符字符串生成指纹。生成一个字符串，放到数据库中作为唯一标示。按照 url 去重，有一个列表，发送请求之前从数据表中看一下这个 url 有没有请求过，请求过了就不用看了 2，内容判断，从数

据库中查数据的表示，如果请求过了就在不在请求了。
4. scrapy和scrapy-redis有什么区别？
scrapy与scrapy_redis感觉最大区别scrapy_redis可持久化爬虫，并支持增量式爬虫，对于分布式爬虫支持很到位

5. 简述你遇到过的反爬机制，你遇到过最棘手的一个是什么，最后如何解决的？
答：前期爬取淘宝商品时，发现动不动就需要人工验证，并且使用电脑点击下一页过快时，也是要滑块认证，当时尝试网上的模拟滑块，当模拟拖拽完成后，依旧被检测出来，显示滑动失败，然后尝试其他方式，都未果，但偶让一次发现，如果你输入网址点击访问，哪怕输入很多次，都不会出现验证，只需找到网址规律，通过selenium加无头浏览器，并通过刷新网址来回避验证
