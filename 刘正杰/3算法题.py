'''
冒泡排序，每一趟排序就像泡泡浮出（将序列最大的元素移动到右边），从左往右，两个两个相比，如果左边元素大于右边，就交换两个元素的位置；
下一趟排序，由于最后一个元素肯定是最大的，只需比较到倒数第二个元素，以此类推
'''


def BubbleSort(list):
    for i in range(len(list) - 1):
        for j in range(len(list) - 1 -i):
            if list[j] > list[i]:
                list[j], list[i] = list[i], list[j]
    return list

'''
选择排序，每一趟选择一个最小元素放在，该躺对应位置，例如第一趟，从下标0到整个列表，找到最小元素放到下表为0的位置，第二趟。从下标1开始，以此类推
'''
def SelectSort(list):
    for i in range(len(list) - 1):
        min = i
        for j in range(i+1,len(list) - 1):
            if list[j] < list[min]:
                min = j
        list[i],list[min] = list[min],list[i]

if __name__ == '__main__':
    list = [8,5,9,4,6,7,10]
    print(BubbleSort(list))
    print(SelectSort(list))
