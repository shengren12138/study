# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from weibo.items import WeiboItem,CommentItem
from pymongo import MongoClient
import datetime
import re

class WeiboPipeline(object):
    def open_spider(self,spider):
        client = MongoClient()
        self.collection1 = client["weibo"]["content"]
        self.collection2 = client["weibo"]["comment"]

    def process_item(self, item, spider):

        if isinstance(item, WeiboItem):
            item["time"] = self.process_time(item["time"])
            item["transpond_count"] = self.process_count(item["transpond_count"])
            item["comment_counts"] = self.process_count(item["comment_counts"])
            item["like_counts"] = self.process_count(item["like_counts"])
            print(item)
            #存入content集合
            #self.collection1.insert(dict(item))
        else:
            self.collection2.insert(dict(item))
        return item
    #处理时间格式
    def process_time(self,time):
        now = datetime.datetime.now()
        return str(time).replace("今天",f"{now.month}月{now.day}日")
    #获取点赞，分享的整数
    def process_count(self,count):
        count=str(count)
        if count.find('[') == -1:
            return 0
        else:
            return int(re.findall(".*?\[(.*?)\]",count)[0])



