# -*- coding: utf-8 -*-
import scrapy
from weibo.items import WeiboItem,CommentItem
from copy import deepcopy
import re



class LenovoSpider(scrapy.Spider):
    name = 'Lenovo'
    allowed_domains = ['weibo.cn']
    start_urls = ['https://weibo.cn/lenovo?']

    def parse(self, response):
        #爬取前20页
        for i in range(1,21):
            url = f'https://weibo.cn/lenovo?page={i}'
            yield scrapy.Request(
                url,
                callback=self.detal_parse
            )
    #处理页面并提取信息
    def detal_parse(self,response):

        div_list = response.xpath('//div[@class="c"]')
        for div in div_list:
            item = WeiboItem()

            text = div.xpath('./div[2]/span[@class="ct"]/text()').extract_first()
            if text:
                item["time"] = str(text).split("来自")[0].replace("\xa0","")
                item["origin"] = str(text).split("来自")[1]
            else:
                continue
            content = div.xpath('./div[1]')
            item["content"] = content[0].xpath('string(.)').extract_first()
            item["transpond_count"] = div.xpath('./div[2]/a[4]/text()').extract_first()
            item["comment_counts"] = div.xpath('./div[2]/a[5]/text()').extract_first()
            item["like_counts"] = div.xpath('./div[2]/a[3]/text()').extract_first()
            item["comment_url"] = div.xpath('./div[2]/a[5]/@href').extract_first()
            #yield item
            if  item["comment_url"] and len(item["comment_url"]) >60 and len(item["comment_url"]) < 65:
                yield scrapy.Request(
                    url=item["comment_url"],
                    callback=self.parse1,
                    meta={"item":deepcopy(item)},
                )

    #处理第一页评论
    def parse1(self,response):
        item = response.meta["item"]
        url = str(item["comment_url"]).replace("#cmtfrm","")
        pages = response.xpath('//div[@id="pagelist"]/form/div/input[1]/@value').extract_first()
        if pages is None:
            page = 1
        else:
            page = int(pages)
        for i in range(1,page+1):
         comment_url = url + f"page={i}"

         yield scrapy.Request(
             comment_url,
             callback=self.next_parse,
             meta={"page":i}
         )

    #提取评论信
    def next_parse(self,response):
        page = response.meta["page"]
        comment_div_list = response.xpath('//div[@class="c"]')
        if page == 1:
            comment_div_list = comment_div_list[1:]
        for div in comment_div_list:
            comment_item = CommentItem()
            #如果评论为空，说明该div标签不是要提取标签
            content = div.xpath('./span[1]/text()').extract_first()
            if content == "[热门]":
                comment_item["text"] = div.xpath('./span[2]/text()').extract_first()
            elif content is None:
                continue
            else:
                comment_item["text"] = content
            people_url = div.xpath('./a[1]/@href').extract_first()
            if len(people_url) > 10 and len(people_url) < 16:
                comment_item["people_id"] = str(people_url).split("/")[-1]
            else:
                continue
            print(people_url)
            comment_item["comme_name"] = str(div.xpath('./a[1]/text()').extract_first())
            yield comment_item

