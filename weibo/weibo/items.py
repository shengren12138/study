# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class WeiboItem(scrapy.Item):
    # define the fields for your item here like:
    #发布内容
    content = scrapy.Field()
    #发布时间
    time = scrapy.Field()
    #发布来源，例如来自微博，来自微博视频等
    origin = scrapy.Field()
    #转发量
    transpond_count = scrapy.Field()
    #评论数目
    comment_counts = scrapy.Field()
    #点赞数目
    like_counts = scrapy.Field()
    #评论url
    comment_url = scrapy.Field()

class CommentItem(scrapy.Item):
    #内容
    text = scrapy.Field()
    # 评论人id
    people_id = scrapy.Field()
    #评论人姓名
    comme_name = scrapy.Field()
